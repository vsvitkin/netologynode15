'use strict';

pokemonApp.controller('CreateBerryCtrl', function($scope, BerriesService) {

    $scope.berry = {};

    $scope.createBerry = function(berry) {

        var newBerryInstance = new BerriesService(berry);
        newBerryInstance.$save({}, function(successResult) {
            $scope.newBerry = {};
            $scope.creationSuccess = true;
        }, function(errorResult) {
            $scope.creationSuccess = false;
        });

    }

});
