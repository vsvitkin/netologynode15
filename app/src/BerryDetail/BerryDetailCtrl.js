'use strict';

pokemonApp.controller('BerryDetailCtrl', function($scope, $routeParams, BerriesService) {

    $scope.berry = BerriesService.get({
        berryId: $routeParams['berryId']
    }, function(successResult) {
        $scope.notfoundError = false;
    }, function(errorResult) {
        $scope.notfoundError = true;
    });

    $scope.deleteBerry = function(berryId) {
      BerriesService.remove({
          'berryId': berryId
      }, function(successResult) {
            $scope.deletionSuccess = true;
        }, function(errorResult) {
            $scope.deletionError = true;
        });

    }

});
