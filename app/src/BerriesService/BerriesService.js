angular
    .module('PokemonApp')
    .factory('BerriesService', function($resource, $http) {
        let myKeys={
            "application-id": "8110BBA4-9A1E-DB1E-FF0E-37689A7B4C00",
            "secret-key": "83B4C68B-C3E2-827F-FFD8-593CCB146600"
        }
        return $resource('https://api.backendless.com/v1/data/berries/:berryId/', {
            berryId: '@berryId',
        }, {
            query: {
                headers: myKeys,
                method: 'GET',
                isArray: true,
                transformResponse: function(responseData) {
                    return angular.fromJson(responseData).data;
                }
            },
            update: {
                headers: myKeys,
                method: 'PUT'
            },
            remove:{
                headers: myKeys,
                method: 'DELETE'
            },
            get:{
                headers: myKeys
            },
            save:{
              headers: myKeys,
              method: 'POST'
            }
        })
    });
